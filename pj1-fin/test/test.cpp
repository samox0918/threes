#include <iostream>
#include <cstdio>
#include <random>
using namespace std;

void show(int *A){
	int i,j;
	for(i=0;i<4;i++){
		for(j=0;j<4;j++){
			cout << A[i*4+j] << " " ;
		}
		cout << endl;
	}
}

int main()
{
	int tile[4][4] = {1,1,6,3,3,12,2,2,1,3,2,3,12,1,3,2};
	show(&tile[0][0]);
	
	//int seq[20] = {1,2,3,6,12,24,48,96,192,384,768,1536,3072,6144};
	for (int r = 0; r < 4; r++) {
		auto& row = tile[r];
		
		int combine_flag = 0;
		
		for (int c = 0; c < 4; c++) {
			cout << row[0] << row[1] << row[2] << row[3] << endl;
			printf("%d %d %d %d\n",combine_flag,r,c,row[c-1]);
			if(!combine_flag && (c == 0 || (c>0 && row[c-1] != 0) ) ){	//左邊卡住了 可以判斷合併
					
				if( c!=3 && (row[c]*row[c+1] == 2 || (row[c] > 2 && row[c]==row[c+1])) ){
						cout << r << c <<"combine"<<endl;
						row[c] = row[c] + row[c+1];
						row[c+1] = 0;
						combine_flag = 1;
				}
			}
			else {
				
				if( row[c-1] == 0){
					cout << r << c << "shift" << endl;
					row[c-1]=row[c];
					row[c] = 0;
				}
			}
		}
	}
	show(&tile[0][0]);
}
